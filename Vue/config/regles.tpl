<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Traitement</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div class="container" style="padding-top: 40px;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h2>Configuration de l'interface</h2>
                <hr>
                <div class="row">
                    <div class="col-md-2">
                        <a class="btn btn-success" id="addRule">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <select class="form-control" name ="ModifierRegle" id="ruleName">
                            <option value="" selected disabled hidden>Modifier une règle</option>
                                <option value="rule1">regle 1</option>
                                <option value="rule2">regle 2</option>
                                <option value="rule3">regle 3</option>
                            </select>
                        </div> 
                    </div>
                </div>
                <div class="row" style="border: 1px black solid; padding: 10px;display: none;" id="addBox">
                    <form method="post" action="index.php?action=config&control=config" id="formAdd">
                        <div class="col-md-4">Type de données : </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select  class="form-control" id="ruleContraint" name ="Type">
                                    <option value="temperature">température</option>
                                    <option value="eau">eau</option>
                                    <option value="nourriture">nourriture</option>
                                    <option value="energie">energie</option>
                                    <option value="humidité">humidité</option>
                                    <option value="vent">vent</option>
                                </select>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="number" name="min" class="form-control" value="0">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center" style="padding-top: 10px;"> <i class="fa fa-angle-left" aria-hidden="true"></i> </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" disabled>
                                            <option selected>valeur</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 text-center" style="padding-top: 10px;"> <i class="fa fa-angle-left" aria-hidden="true"></i> </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="number" name="max" class="form-control" value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">Nom de la règle : </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="titre" class="form-control">
                                    </div> 
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-4">état : </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" name="état" class="form-control">
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6 text-right">
                                    <button class="btn btn-success" type="submit">
                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                    </button>
                                    <button class="btn btn-danger" id="resetBtn">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

               

            </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        var active = "";

        $("#addRule").click(function(){
            $("#addBox").show();
        });

        $("#resetBtn").click(function(){
            $("#addBox").hide();
            $('#formAdd').trigger("reset");
        });

        $("#ruleName").change(function() {
            var id = $("#ruleName option:selected").val();
            console.log(id);
            
            if(active != ""){
                $(active).hide();
            }

            $("#"+id).show();
            active = id;
        });
    </script>
</body>
</html>