<html>
<?php session_start();?>
    <head>
        <title>Accueil</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./Vue/asset/css/bootstrap.min.css">
        <script src="./Vue/asset/js/jquery.min.js"></script>
        <script src="./Vue/asset/js/jquery.min.js"></script>
        <script src="./Vue/accueil/js/accueil.js"></script>
        <link rel="stylesheet" href="./Vue/accueil/css/accueil.css">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
        integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
        integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
        crossorigin=""></script>
        
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 log">
                    <div class="widget">
                        <h1 class="titre">Journal de bord</h1>
                        <div id="log-text">
                            <p>lalaalalalalalalal</p>
                        </div>
                        
                        <a href="index.php?controle=journal&action=voir" class="btn btn-block btn-default" id="voir">Voir</a>
                        
                        <a href="index.php?controle=journal&action=ajouter" class="btn btn-block btn-default" id="ajouter">Ajouter</a>
                        
                    </div>
                </div>
                <div class="col-md-6 col-menu">
                    <div class="row">
                        <div class="col-md-6 map">
                            <div class="widget">
                                <div class="titre">
                                        <h1>Carte</h1>
                                </div>
                                <div class="col-md-12" id="carte">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 sondes">
                            <div class="widget">
                                <div class="row" >
                                    <div class="col-md-12 titre" id="titre-sondes">
                                            <h1>Sondes</h1>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <a href="./index.php?controle=sonde&action=graph&param=humidite" class="col-md-12 annonce-sondes">
                                            <div class="ligne"></div>
                                            <h3 style="text-aligne:center;">Humidité  </h3>
                                            <!-- pas de pluie (depuis 300 jours) -->
                                    </a>
                                </div>
                                
                                <div class="row">
                                    <a href="./index.php?controle=sonde&action=graph&param=temperature" class="col-md-12 annonce-sondes">
                                        
                                            <div class="ligne"></div>
                                            <h3 style="text-aligne:center;">Température  </h3>
                                            <!-- il fait chaud -->
                                    </a>
                                </div>
                                
                                <div class="row">
                                    <a href="./index.php?controle=sonde&action=graph&param=aridite" class="col-md-12 annonce-sondes">
                                            <div class="ligne"></div>
                                           <h3 style="text-aligne:center;">Aridité </h3>
                                            <!-- c'est le désert -->
                                    </a>
                                </div>
                                
                                 <div class="row">
                                    <a href="./index.php?controle=sonde&action=graph&param=seisme" class="col-md-12 annonce-sondes">
                                            <div class="ligne"></div>
                                            <h3 style="text-aligne:center;">Capteur sismique  </h3>
                                            <!-- honnettement si elle dépasse 4 on est mal  -->
                                            
                                    </a>
                                </div>
                                
                                <div class="row">
                                    <a href="./index.php?controle=sonde&action=graph&param=vitesse_Vent" class="col-md-12 annonce-sondes">
                                        <div class="ligne"></div>
                                            <h3 style="text-aligne:center;">Capteur vent  </h3>
                                            <!-- une petite brise -->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-12 stocks">
                            <div class="widget">
                                <div class="titre">
                                    <h1>Stock</h1>
                                </div>
                                <div class="ligne"></div>
                                <div class="row" id="stock-col">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 titre">
                                        <h3>Eau</h3>
                                        <div class="progress vertical p-stock" id="full-bar-blue">
                                            <div class="empty-bar" id="eau"></div>
                                        </div>
                                        <h3>70%</h3>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 titre">
                                        <h3>Nourriture</h3>
                                        <div class="progress vertical p-stock" id="full-bar-green">
                                            <div class="empty-bar" id="nourriture"></div>
                                        </div>
                                        <h3>40%</h3>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 titre">
                                        <h3>Energie</h3>
                                        <div class="progress vertical p-stock" id="full-bar-yellow">
                                            <div class="empty-bar" id="energie"></div>
                                        </div>
                                        <h3>10%</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            getLastJournal();
            var mymap = L.map('carte').fitWorld();
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiYWhjZW5lIiwiYSI6ImNqcGRibHhrNDM1ajgza3Boenc5bG43NXkifQ.EOoNIuIZDHgC1ceJexgyrg'
            }).addTo(mymap);
            mymap.locate({setView: true, maxZoom: 16});

            function onLocationFound(e) {
                var radius = e.accuracy / 2;
                
                L.marker(e.latlng).addTo(mymap)
                    .bindPopup("Vous etes au autour de " + radius + " metres de ce point").openPopup();

                L.circle(e.latlng, radius).addTo(mymap);
            }

            mymap.on('locationfound', onLocationFound);
        </script>
    </body>
</html>