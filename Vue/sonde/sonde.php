<html>
    <head>
        <title>Sonde</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./Vue/asset/css/bootstrap.min.css">
        <script src="./Vue/asset/js/jquery.min.js"></script>
        <script src="./Vue/asset/js/bootstrap.min.js"></script>
        <script src="./Vue/sonde/js/sonde.js"></script>
        <link rel="stylesheet" href="./Vue/sonde/css/styles_sonde.css">
        <script>
            window.onload = function() {
            
            console.log('<?=$_GET['param']?>')
            getSonde('<?=$_GET['param']?>');
            }
        </script>
    </head>
    <body>			
		<section class = "container-fluid">
			<div class = "row">
				<div class="text-center col-md-6" id = "nom">
                    <h3><?=$_GET['param']?></h3>
				</div>
				
			
			</div>
			<div class = "container">
				<div class="text-center col-md-12" id = "nom">
                    <div id="chartContainer" style="height: 680px; width: 100%;"></div>
                </div>
			</div>
		</section>
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    </body>
</html>