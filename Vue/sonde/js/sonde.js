var chart
var dataPoints = [];
function getSonde(capteur){
    console.log("test")
    $.ajax({
        url: './Modele/getSonde.php',
        type: 'POST',
        processData: false,
        contentType: false,
        dataType: "json",
        success: function (res) {
            if (res.error) {
                console.log(res)
                
                
            } else {
                console.log(res)
                

                chart = new CanvasJS.Chart("chartContainer", {
                    animationEnabled: true,
                    theme: "light2",
                    zoomEnabled: true,
                    title: {
                        text: "Graphique des relevés : "
                    },
                    axisY: {
                        title: capteur,
                        titleFontSize: 24,
                        prefix: ""
                    },
                    data: [{
                        type: "line",
                        yValueFormatString: "#,##0.00",
                        dataPoints: dataPoints
                    }]
                });
                addData(res, capteur);
                //
            }
        },
        error: function (error) {
            console.log(error);
        }
    })
}



function addData(data, capteur) {
    
    for (var i = 0; i < data.length; i++) {
        console.log(Date(data[i]['date']));
        dataPoints.push({
            x: new Date(data[i]['date'].split("/")[2]+'-'+data[i]['date'].split("/")[1]+'-'+data[i]['date'].split("/")[0]),
            y: parseInt(data[i][capteur])
        });
    }
    chart.render();
}

            