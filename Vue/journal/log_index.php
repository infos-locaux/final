<!DOCTYPE html>
	<html>
		<head>
			<title>PLB</title>
			
			<meta charset="utf-8">
			  
			<meta name="viewport" content="width=device-width, initial-scale=1">
			
			<link rel="stylesheet" href="./Vue/asset/css/bootstrap.min.css">
			
			<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
			
			<script src="./Vue/asset/js/jquery.min.js"></script>
			
			<script src="./Vue/asset/js/bootstrap.min.js"></script>
			
			<!-- CSS -->
			<link rel="stylesheet" type="text/css" href="./Vue/journal/css/log_index.css" />
			
		</head>
		
		<body>
			<section class="container">
				<div class="row">
					<div class="col-md-2">
						<button class="btn btn-block btn-default" onclick="location.href='./index.php';">
							Retour
						</button>
					</div>
					
					<div class="col-md-10">
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-block btn-default" onclick="location.href='./index.php?controle=journal&action=ajouter';">
								Ajouter
								</button>
							</div>
							<div class="col-md-12 logs">
								<div id="accordion" class="panel-group">
									<?php
									for($i = 0; $i < count($journal); $i++){
									?>
									<div class="panel">
									  <div class="panel-heading">
									  <h4 class="panel-title">
										<a href="#panelBody<?=$i?>" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"><?=$journal[$i]['date'] ?><span class="btn-edit" onclick="location.href = 'log_edit.html';"><i class="fa fa-pencil-square-o"></i></span></a>
										</h4>
									  </div>
									  <div id="panelBody<?=$i?>" class="panel-collapse collapse">
									  <div class="panel-body">
										  <p><?= $journal[$i]['texte']?></p>
										</div>
									  </div>
									</div>
									<?php } ?>
								  </div>
							</div>
						</div>
					</div>
					
				</div>
			
			</section>
		
		</body>
		
	</html>