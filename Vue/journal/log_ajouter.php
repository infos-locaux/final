<!DOCTYPE html>
	<html>
		<head>
			<title>PLB</title>
			
			<meta charset="utf-8">
			  
			<meta name="viewport" content="width=device-width, initial-scale=1">
			
			<link rel="stylesheet" href="./Vue/asset/css/bootstrap.min.css">
			
			<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
			
			<script src="./Vue/asset/js/jquery.min.js"></script>
			
			<script src="./Vue/asset/js/bootstrap.min.js"></script>
			<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
			<!-- CSS -->
			<link rel="stylesheet" type="text/css" href="./Vue/journal/css/log_ajouter.css" />
			
			
		</head>
		
		<body>
			<section class="container">
				<div class="row">
					<div class="col-md-2">
				        <button class="btn btn-block btn-default" onclick="location.href='./index.php';">Retour</button>
					</div>
					
					<div class="col-md-10">
						<div class="row">
							<div class="col-md-12">
								<hr />
								<form action="./index.php?controle=journal&action=ajoutJournal" method="post">
									<textarea name="editor1" id="editor1" rows="10" cols="80">
										
									</textarea>
								  <button type="submit" class="btn btn-primary btn-block">Sauvegarder</button>
								</form>
							</div>
						</div>
					</div>
					
				</div>
			
			</section>
			<script>
			$(document).ready(function() {
			  CKEDITOR.replace( 'editor1' );
			});
			</script>
		</body>
		
	</html>