-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 07 Décembre 2018 à 07:49
-- Version du serveur :  5.6.20-log
-- Version de PHP :  5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `infos`
--

-- --------------------------------------------------------

--
-- Structure de la table `journal`
--

CREATE TABLE IF NOT EXISTS `journal` (
`id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `texte` text NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=44 ;

--
-- Contenu de la table `journal`
--

INSERT INTO `journal` (`id`, `date`, `texte`, `longitude`, `latitude`) VALUES
(1, 'ddfgd', 'dfgdfgd', 1.55, 1.56),
(2, 'ddfgd', 'dfgdfgd', 1.55, 1.56),
(3, 'ddfgd', 'dfgdfgd', 1.55, 1.56),
(4, '07-12-2018', '<p>testqd</p>\r\n', 1.55, 1.56),
(5, '07-12-2018', '<p>testqd</p>\r\n', 1.55, 1.56),
(6, '07-12-2018', '<p>dfgdf</p>\r\n', 1.55, 1.56),
(7, '07-12-2018', '<p>dfgdf</p>\r\n', 1.55, 1.56),
(8, '07-12-2018', '<p>sdfgdfgdf</p>\r\n', 1.55, 1.56),
(9, '07-12-2018', '<p>sdfgdfgdf</p>\r\n', 1.55, 1.56),
(10, '07-12-2018', '<p>dfvfd</p>\r\n', 1.55, 1.56),
(11, '07-12-2018', '<p>dwsfcxd</p>\r\n', 1.55, 1.56),
(12, '07-12-2018', '<p>xvxcv</p>\r\n', 1.55, 1.56),
(13, '07-12-2018', '<p>sdfds</p>\r\n', 1.55, 1.56),
(14, '07-12-2018', '<p>sdfdssdfsd</p>\r\n', 1.55, 1.56),
(15, '07-12-2018', '<p>sdfsd</p>\r\n', 1.55, 1.56),
(16, '07-12-2018', '<p>sdfsd</p>\r\n', 1.55, 1.56),
(17, '07-12-2018', '<p>sdfsd</p>\r\n', 1.55, 1.56),
(18, '07-12-2018', '<p>xcvx</p>\r\n', 1.55, 1.56),
(19, '07-12-2018', '<p>sdfdsf</p>\r\n', 1.55, 1.56),
(20, '07-12-2018', '<p>xc xv</p>\r\n', 1.55, 1.56),
(21, '07-12-2018', '<p>dwcx</p>\r\n', 1.55, 1.56),
(22, '07-12-2018', '<p>dwcx</p>\r\n', 1.55, 1.56),
(23, '07-12-2018', '<p>dwcxdxvxc</p>\r\n', 1.55, 1.56),
(24, '07-12-2018', '<p>sdf</p>\r\n', 1.55, 1.56),
(25, '07-12-2018', '<p>sdfsdf</p>\r\n', 1.55, 1.56),
(26, '07-12-2018', '<p>sqdqsd</p>\r\n', 1.55, 1.56),
(27, '07-12-2018', '', 1.55, 1.56),
(28, '07-12-2018', '', 1.55, 1.56),
(29, '07-12-2018', '', 1.55, 1.56),
(30, '07-12-2018', '', 1.55, 1.56),
(31, '07-12-2018', '', 1.55, 1.56),
(32, '07-12-2018', '', 1.55, 1.56),
(33, '07-12-2018', '', 1.55, 1.56),
(34, '07-12-2018', '', 1.55, 1.56),
(35, '07-12-2018', '<p>xcvcx</p>\r\n', 1.55, 1.56),
(36, '07-12-2018', '<ol>\r\n	<li>Survie</li>\r\n	<li>Rationnement</li>\r\n	<li>Recherche d&#39;eau</li>\r\n	<li>recherche et developpement</li>\r\n	<li>...</li>\r\n</ol>\r\n', 1.55, 1.56),
(37, '07-12-2018', '<p>zszszszszsz</p>\r\n', 1.55, 1.56),
(38, '07-12-2018', '<ol>\r\n	<li>szszszszs</li>\r\n	<li>dedededede</li>\r\n	<li>szszsz</li>\r\n</ol>\r\n', 1.55, 1.56),
(39, '07-12-2018', '<p>dzdzdzdz</p>\r\n', 1.55, 1.56),
(40, '07-12-2018', '<ul>\r\n	<li>frfrfrfr</li>\r\n	<li>ffrfrfr</li>\r\n	<li>dededed</li>\r\n</ul>\r\n', 1.55, 1.56),
(41, '07-12-2018', '<p>ezfezfefezfzffez</p>\r\n', 1.55, 1.56),
(42, '07-12-2018', '<p>dzdzdzdzdzdz</p>\r\n\r\n<ul>\r\n	<li>dzdzdzdzd</li>\r\n	<li>zdzdzdzdzdz</li>\r\n</ul>\r\n\r\n<p>dzdzdz</p>\r\n', 1.55, 1.56),
(43, '07-12-2018', '<ol>\r\n	<li>dedzdzdzd</li>\r\n	<li>ededededede</li>\r\n</ol>\r\n\r\n<p>fefefefefef</p>\r\n\r\n<ul>\r\n	<li>dzdzdzdzd</li>\r\n	<li>zdzdzdz</li>\r\n</ul>\r\n', 1.55, 1.56);

-- --------------------------------------------------------

--
-- Structure de la table `sonde`
--

CREATE TABLE IF NOT EXISTS `sonde` (
`id` int(11) NOT NULL,
  `aridite` float NOT NULL,
  `temperature` float NOT NULL,
  `humidite` float NOT NULL,
  `vitesse_Vent` float NOT NULL,
  `seisme` float NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `sonde`
--

INSERT INTO `sonde` (`id`, `aridite`, `temperature`, `humidite`, `vitesse_Vent`, `seisme`, `latitude`, `longitude`, `actif`, `date`) VALUES
(1, 2, 5, 4, 3, 12, 45.54, 24.45, 1, '12/01/2018'),
(2, 5, 3, 6, 1, 4, 5, 6, 1, '15/12/2018');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `prenom`, `email`, `pass`) VALUES
(1, 'ait', 'ahcene', 'email@test.fr', 'testpassword');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `journal`
--
ALTER TABLE `journal`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sonde`
--
ALTER TABLE `sonde`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `journal`
--
ALTER TABLE `journal`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT pour la table `sonde`
--
ALTER TABLE `sonde`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
