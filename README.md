# front du projet 
## techs used :
- php vanilla (without framework)
- mvc

## how to clone the project :
- git clone https://gitlab.com/infos-locaux/back
- **copy** and **rename** connexion.php.dist to connexion.php
- edit connexion.php to match your database login, password and format
- use info.sql and nuitdelinfo.sql to put the entries in your database
- move the project in an apache server